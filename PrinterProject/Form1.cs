﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrinterProject
{
    public partial class Form1 : Form
    {
        private string stringToPrint;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (FileStream stream = new FileStream(this.openFileDialog1.FileName, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        stringToPrint = reader.ReadToEnd();
                    }
                }
            }


            DialogResult result = this.printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                printDocument1.Print();
            }

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int charactersOnPage = 0;
            int linesPerPage = 0;

            // Sets the value of charactersOnPage to the number of characters 
            // of stringToPrint that will fit within the bounds of the page.
            e.Graphics.MeasureString(stringToPrint, this.Font,
                e.MarginBounds.Size, StringFormat.GenericTypographic,
                out charactersOnPage, out linesPerPage);

            // Draws the string within the bounds of the page
            e.Graphics.DrawString(stringToPrint, this.Font, Brushes.Black,
                e.MarginBounds, StringFormat.GenericTypographic);

            // Remove the portion of the string that has been printed.
            stringToPrint = stringToPrint.Substring(charactersOnPage);

            // Check to see if more pages are to be printed.
            e.HasMorePages = (stringToPrint.Length > 0);
        }

        private void printDocument1_EndPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //MessageBox.Show(this.printDocument1.DocumentName + " has finished printing.");
        }

        private void printDocument1_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DataRow row = this.printerDataSet.Tables[0].NewRow();
            row["memo"] = this.openFileDialog1.FileName;
            this.printerDataSet.Tables[0].Rows.Add(row);
            this.tableAdapterManager.UpdateAll(this.printerDataSet);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'printerDataSet.logs' table. You can move, or remove it, as needed.
            this.logsTableAdapter.Fill(this.printerDataSet.logs);

        }
    }
}
